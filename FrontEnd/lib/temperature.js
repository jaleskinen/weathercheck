
$(document).ready(function () {
    
    //Load default google maps with Finland center point coordinates.  
    navigator.geolocation.getCurrentPosition(success, error);
    function success(position) {
        var lat_default = 64.96;
        var lng_default = 27.59;
        var latlng = new google.maps.LatLng(lat_default, lng_default);
        var mapProp = {
            center: latlng,
            zoom: 4,
            mapTypeId: google.maps.MapTypeId.HYBRID
        };
        var map = new google.maps.Map(document.getElementById("my_map"), mapProp);
    }
    
    //Alert to user if geolocation support missing.
    function error() {
		//alert("Geolocation not supported by browser");
        console.log("Geolocation maybe not supported by browser? IE and Firefox may fail even supported, so we will try open map anyway...");
        var lat_default = 64.96;
        var lng_default = 27.59;
        var latlng = new google.maps.LatLng(lat_default, lng_default);
        var mapProp = {
            center: latlng,
            zoom: 4,
            mapTypeId: google.maps.MapTypeId.HYBRID
        };
        var map = new google.maps.Map(document.getElementById("my_map"), mapProp);
	}

    $('#temperature').submit(function () {
        //Prevent default Java function (success Alert)
        event.preventDefault();
        
        //Store search address
        var my_address = $("input:first").val();

        var geocoder = new google.maps.Geocoder();
        var geoCodeRequest = {
            address: my_address
        };
        
        //Load google maps to desired address
        geocoder.geocode(geoCodeRequest, function (response, status) {

            if (status === google.maps.GeocoderStatus.OK) {
                var data = response[0];
                var lat = data.geometry.location.lat();
                var lng = data.geometry.location.lng();
                var latlng = new google.maps.LatLng(lat, lng);

                //Create wunderground search address with API key
                var wu_url = "http://api.wunderground.com/api/9f4c9f5718842fd6/geolookup/conditions/q/" + lat + ',' + lng + ".json";
                console.log(wu_url);
                //Ajax query to wu_url, returns json object
                $.ajax({
                    url : wu_url,
                    dataType : "jsonp",
                    success : function (parsed_json) {
                        //Parse wanted data out from the json object
                        var location = parsed_json['location']['city'];
                        var observation_location = parsed_json['current_observation']['observation_location']['city'];
                        var observation_url = parsed_json['current_observation']['ob_url'];
                        console.log(observation_url);
                        var lat_parsed = parsed_json['current_observation']['observation_location']['latitude'];
                        var lng_parsed = parsed_json['current_observation']['observation_location']['longitude'];
                        var temp_c = parsed_json['current_observation']['temp_c'];

                        //Create new google map, marker and detected temperature value
                        var latlng = new google.maps.LatLng(lat_parsed, lng_parsed);
                        var mapProp = {
                            center: latlng,
                            zoom: 10,
                            mapTypeId: google.maps.MapTypeId.HYBRID
                        };
                        var map = new google.maps.Map(document.getElementById("my_map"), mapProp);
                        //Set marker to weather station address
                        var marker = new google.maps.Marker({
                            position: latlng,
                            map: map,
                            visible: true
                        });
                        var infowindow = new google.maps.InfoWindow({
                           content: observation_location + ": " + temp_c + " &deg;" + "C  "
                         });
                        infowindow.open(map,marker);
                        //Add observation url link listener to google maps marker
                        google.maps.event.addListener(marker, 'click', function() {
                            window.open(observation_url);
                        });
                    }
                });
            }
        });
    });
});



