var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var http = require('http');
var app = express();

app.set('port', process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 3000);
app.set('ip', process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1");

//============ MIDDLEWARES =================

app.use(function (req, res, next) {

    next();
});

//bodyParser urlencoded() middleware parses the json object from HTTP POST request
app.use(bodyParser.urlencoded());
app.use('/', express.static(path.join(__dirname, '/FrontEnd/view/')));
app.use('/FrontEnd/css', express.static(path.join(__dirname, '/FrontEnd/css')));
app.use('/FrontEnd/lib', express.static(path.join(__dirname, '/FrontEnd/lib')));

//============ ROUTERS======================

http.createServer(app).listen(app.get('port'), app.get('ip'), function () {
    console.log("Express server started");
});